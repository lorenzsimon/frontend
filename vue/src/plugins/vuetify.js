import Vue from "vue";
import Vuetify from "vuetify";
import "vuetify/dist/vuetify.min.css";

Vue.use(Vuetify);

export default new Vuetify({
  icons: {
    iconfont: "md"
  },
  theme: {
    themes: {
      light: {
        primary: "#226893",
        secondary: "#bdbdbd",
        accent: "#229386",
        error: "#a90a0a",
        warning: "#fbc02d",
        success: "#66b62f",
        info: "#229386",
        background: "#fff"
      },
      dark: {
        primary: "#497996",
        secondary: "#cacaca",
        accent: "#3e867e",
        error: "#942E2E",
        warning: "#ACAC3C",
        success: "#5C893D",
        info: "#3e867e",
        background: "#272727"
      }
    }
  }
});
