import i18n from "@/i18n";

const rules = {
  data() {
    return {
      required: [v => !!v || i18n.t("rules.required")],
      emailRules: [
        v => !!v || i18n.t("rules.emailRules1"),
        v => /.+@.+\..+/.test(v) || i18n.t("rules.emailRules2")
      ],
      passwordRules: [v => !!v || i18n.t("rules.passwordRules")],
      nameRules: [
        v => !!v || i18n.t("rules.nameRules1"),
        v => (v && v.length <= 20) || i18n.t("rules.nameRules2")
      ],
      agree: [v => !!v || i18n.t("rules.agree")]
    };
  }
};
export default rules;
