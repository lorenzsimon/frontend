import i18n from "@/i18n";

export default {
  data: () => {
    return {
      exampleQueries: [
        {
          name: i18n.t("search.example3"),
          query: `SELECT ?tonalityLabel (
    COUNT( ?tonalityLabel ) as ?count
)
WHERE { ?work wdt:P826 ?tonality.
    SERVICE wikibase:label {
    bd:serviceParam wikibase:language 'en'
    }
}
GROUP BY ?tonalityLabel
ORDER BY DESC( ?count )`
        },
        {
          name: i18n.t("search.example2"),
          query: `SELECT ?item ?itemLabel
WHERE { ?item wdt:P21 wd:Q6581072 .
    { ?item wdt:P19 wd:Q730}
    UNION  {
    ?item wdt:P19 ?pob . ?pob wdt:P131* wd:Q730
    }
    OPTIONAL {
    ?sitelink schema:about ?item . ?sitelink schema:inLanguage 'de'
    }
    FILTER ( !BOUND ( ?sitelink )) ?item wdt:P31 wd:Q5 .
    SERVICE wikibase:label {
    bd:serviceParam wikibase:language '[AUTO_LANGUAGE]' .
    }
}
LIMIT 20 `
        },
        {
          name: i18n.t("search.example1"),
          query: `SELECT ?King ?KingLabel
WHERE {
    SERVICE wikibase:label {
    bd:serviceParam wikibase:language '[AUTO_LANGUAGE],en'.
    }
    ?King wdt:P31 wd:Q5;
    wdt:P27 wd:Q34;
    wdt:P106 wd:Q116.
}
LIMIT 50`
        }
      ]
    };
  }
};
