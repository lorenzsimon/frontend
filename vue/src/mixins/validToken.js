export default {
  computed: {
    valid() {
      const token = this.$cookies.get("session_token");

      let base64Url = token.split(".")[1];
      let base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
      let jsonPayload = decodeURIComponent(
        atob(base64)
          .split("")
          .map(function(c) {
            return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
          })
          .join("")
      );

      const tokenData = JSON.parse(jsonPayload);

      let t = new Date(1970, 0, 1, 1, 0);

      // setting the validation to 20 seconds earlier to prevent a fail due to invalid instant after checking
      t.setSeconds(tokenData.exp - 20);

      let now = new Date();

      return t.getTime() > now.getTime();
    },
    isOAuthToken() {
      const token = this.$cookies.get("refresh_token");

      let base64Url = token.split(".")[1];
      let base64 = base64Url.replace(/-/g, "+").replace(/_/g, "/");
      let jsonPayload = decodeURIComponent(
        atob(base64)
          .split("")
          .map(function(c) {
            return "%" + ("00" + c.charCodeAt(0).toString(16)).slice(-2);
          })
          .join("")
      );

      const tokenData = JSON.parse(jsonPayload);

      return tokenData.isOAuth;
    }
  }
};
