import Vue from "vue";
import VueRouter from "vue-router";
import i18n from "@/i18n";
import store from "../store";
import cookies from "vue-cookies";
// import axios from "axios";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    redirect: `/${i18n.locale}`
  },
  {
    path: "/:lang",
    component: {
      render(c) {
        return c("router-view");
      }
    },
    children: [
      {
        path: "/",
        name: "Search",
        component: () => import("@/components/Search.vue")
      },
      {
        path: "login",
        name: "Login",
        component: () => import("@/components/Login.vue")
      },
      {
        path: "oauthlogin",
        name: "OAuth",
        component: () => import("@/components/OAuth.vue")
      },
      {
        path: "register",
        name: "Register",
        component: () => import("@/components/Register.vue")
      },
      {
        path: "faq",
        name: "FAQ",
        component: () => import("@/components/FAQ.vue")
      },
      {
        path: "forum",
        name: "Forum",
        component: () => import("@/components/Forum.vue")
      },
      {
        path: "saved",
        name: "Saved",
        component: () => import("@/components/Saved.vue")
      },
      {
        path: "profile",
        name: "Profile",
        component: () => import("@/components/Profile.vue")
      },
      {
        path: "changepassword",
        name: "ChangePassword",
        component: () => import("@/components/ChangePassword.vue")
      },
      {
        path: "deleteaccount",
        name: "DeleteAccount",
        component: () => import("@/components/DeleteAccount.vue")
      },
      {
        path: "impressum",
        name: "Impressum",
        component: () => import("@/components/Impressum.vue")
      },
      {
        path: "datapolicy",
        name: "DataPolicy",
        component: () => import("@/components/DataPolicy.vue")
      },
      {
        path: "admin",
        name: "Admin",
        component: () => import("@/components/Admin.vue")
      },
      {
        path: "*",
        name: "Error",
        component: () => import("@/components/Error.vue")
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes
});

router.beforeEach((to, from, next) => {
  let lang = to.params.lang;
  if (cookies && cookies.get("session_token")) {
    const token = cookies.get("session_token");
    store.dispatch("setUserToken", token);
    store.dispatch("setLoggedIn", true);
  }

  if (
    (to.name === "Profile" ||
      to.name === "Saved" ||
      to.name === "DeleteAccount" ||
      to.name === "ChangePassword") &&
    !store.getters.userToken
  )
    next({ name: "Login", params: { lang: lang } });
  else next();
});

export default router;
