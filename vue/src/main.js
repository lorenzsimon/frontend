import Vue from "vue";
import "./plugins/axios";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import VueCookies from "vue-cookies";
import "@babel/polyfill";
import i18n from "./i18n";

Vue.config.productionTip = false;
Vue.use(VueCookies);

router.beforeEach((to, from, next) => {
  let language = to.params.lang;
  if (!language) {
    language = "de";
  }
  i18n.locale = language;
  next();
});

new Vue({
  router,
  store,
  vuetify,
  i18n,
  render: h => h(App)
}).$mount("#app");
