import Vue from "vue";
import Vuex from "vuex";
import i18n from "@/i18n";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    loggedIn: false,
    userToken: "",
    userName: "",
    userId: "",
    isAdmin: false,
    passwordHash: "",
    userMail: "",
    searchSparqlQuery: false,
    activeQuery: "",
    errorStatus: i18n.t("error.page"),
    errorMessage: i18n.t("error.message"),
    queries: [
      // This is only for offline mocking (gets replaced by API data)
      {
        id: "_QUERY1_",
        sparql: true,
        name: i18n.t("search.example3"),
        content:
          "SELECT ?tonalityLabel ( \
            COUNT( ?tonalityLabel ) as ?count \
          ) \
          WHERE { ?work wdt:P826 ?tonality. \
            SERVICE wikibase:label {\
              bd:serviceParam wikibase:language 'en' \
            } \
          } \
          GROUP BY ?tonalityLabel \
          ORDER BY DESC( ?count )",
        date: Date.now()
      },
      {
        id: "_QUERY2_",
        sparql: true,
        name: i18n.t("search.example2"),
        content: `SELECT ?item ?itemLabel \
          WHERE { ?item wdt:P21 wd:Q6581072 . \
            { ?item wdt:P19 wd:Q730}\
            UNION  { \
              ?item wdt:P19 ?pob . ?pob wdt:P131* wd:Q730 \
            } \
            OPTIONAL { \
              ?sitelink schema:about ?item . ?sitelink schema:inLanguage 'de' \
            } \
            FILTER ( !BOUND ( ?sitelink )) ?item wdt:P31 wd:Q5 . \
            SERVICE wikibase:label { \
              bd:serviceParam wikibase:language '[AUTO_LANGUAGE]' . \
            } \
          } \
          LIMIT 20 `,
        date: Date.now()
      },
      {
        id: "_QUERY3_",
        sparql: true,
        name: i18n.t("search.example1"),
        content: `SELECT ?King ?KingLabel \
          WHERE { \
            SERVICE wikibase:label { \
              bd:serviceParam wikibase:language '[AUTO_LANGUAGE],en'. \
            } \
            ?King wdt:P31 wd:Q5; \
            wdt:P27 wd:Q34; \
            wdt:P106 wd:Q116. \
          } \
          LIMIT 50`,
        date: Date.now()
      }
    ]
  },
  actions: {
    setActiveQuery({ commit }, query) {
      commit("SET_ACTIVE_QUERY", query);
    },
    setLoggedIn({ commit }, data) {
      commit("SET_LOGGEDIN", data);
    },
    setUserToken({ commit }, data) {
      commit("SET_USER_TOKEN", data);
    },
    setUserName({ commit }, data) {
      commit("SET_USER_NAME", data);
    },
    setUserId({ commit }, data) {
      commit("SET_USER_ID", data);
    },
    setUserMail({ commit }, data) {
      commit("SET_USER_MAIL", data);
    },
    setAdmin({ commit }, data) {
      commit("SET_ADMIN", data);
    },
    setSearchSparqlQuery({ commit }, data) {
      commit("SET_SEARCH_SPARQL_QUERY", data);
    }
  },
  mutations: {
    SET_ACTIVE_QUERY(state, query) {
      state.activeQuery = query;
    },
    SET_LOGGEDIN(state, status) {
      state.loggedIn = status;
    },
    SET_ADMIN(state, status) {
      state.isAdmin = status;
    },
    SET_USER_TOKEN(state, token) {
      state.userToken = token;
    },
    SET_USER_NAME(state, name) {
      state.userName = name;
    },
    SET_USER_ID(state, id) {
      state.userId = id;
    },
    SET_USER_MAIL(state, mail) {
      state.userMail = mail;
    },
    SET_SEARCH_SPARQL_QUERY(state, bool) {
      state.searchSparqlQuery = bool;
    }
  },
  getters: {
    activeQuery: state => state.activeQuery,
    searchSparqlQuery: state => state.searchSparqlQuery,
    loggedIn: state => state.loggedIn,
    userToken: state => state.userToken,
    userName: state => state.userName,
    userId: state => state.userId,
    passwordHash: state => state.passwordHash,
    userMail: state => state.userMail,
    errorStatus: state => state.errorStatus,
    errorMessage: state => state.errorMessage,
    isAdmin: state => state.isAdmin
  },
  modules: {}
});
