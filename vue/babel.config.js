module.exports = {
  presets: ["@vue/cli-plugin-babel/preset"],
  plugins: [
    [
      "prismjs",
      {
        languages: ["javascript", "css", "sparql"],
        plugins: ["line-numbers", "line-highlight"],
        theme: "default",
        css: true
      }
    ]
  ]
};
