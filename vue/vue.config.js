module.exports = {
  css: {
    loaderOptions: {
      sass: {
        additionalData: `
          @import "@/assets/css/_variables.scss";
          @import "@/assets/css/_content.scss";
          @import "@/assets/css/_passwordCheck.scss";
        `
      }
    }
  },

  pluginOptions: {
    i18n: {
      locale: "(de)",
      fallbackLocale: "en",
      localeDir: "locales",
      enableInSFC: false
    }
  }
};
