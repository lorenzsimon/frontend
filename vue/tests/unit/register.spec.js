import Vue from "vue";
import Vuex from "vuex";
import { state, actions } from "@/store/index";
import Vuetify from "vuetify";
import { mount, createLocalVue } from "@vue/test-utils";
import Register from "@/components/Register.vue";

Vue.use(Vuetify);

const localVue = createLocalVue();

describe("module", () => {
  let store;
  let vuetify;
  beforeEach(() => {
    vuetify = new Vuetify();
    store = new Vuex.Store({
      getters: {
        loggedIn: () => false,
        activeQuery: () => ""
      },
      state,
      actions
    });
  });

  it("creates component", () => {
    const wrapper = mount(Register, {
      localVue,
      vuetify,
      store
    });
    expect(wrapper.find(".default-border").exists()).toBe(true);
  });
});
