import Vue from "vue";
import Vuex from "vuex";
import VueRouter from "vue-router";
import { state, actions } from "@/store/index";
import Vuetify from "vuetify";
import { mount, createLocalVue } from "@vue/test-utils";
import Profile from "@/components/Profile.vue";

Vue.use(Vuetify);

const localVue = createLocalVue();
localVue.use(VueRouter);

describe("module", () => {
  let store;
  let vuetify;
  let router;
  beforeEach(() => {
    vuetify = new Vuetify();
    router = new VueRouter();
    store = new Vuex.Store({
      getters: {
        userName: () => "",
        userMail: () => ""
      },
      state,
      actions
    });
  });

  it("creates component", () => {
    const wrapper = mount(Profile, {
      localVue,
      vuetify,
      store,
      router
    });
    expect(wrapper.find(".default-border").exists()).toBe(true);
  });
});
