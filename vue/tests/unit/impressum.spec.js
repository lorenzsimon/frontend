import Vue from "vue";
import Vuex from "vuex";
import { state, actions } from "@/store/index";
import Vuetify from "vuetify";
import { mount, createLocalVue } from "@vue/test-utils";
import Impressum from "@/components/Impressum.vue";

Vue.use(Vuetify);

const localVue = createLocalVue();

describe("module", () => {
  let store;
  let vuetify;
  beforeEach(() => {
    vuetify = new Vuetify();
    store = new Vuex.Store({
      getters: {
        loggedIn: () => false,
        activeQuery: () => ""
      },
      state,
      actions
    });
  });

  it("creates component", () => {
    const wrapper = mount(Impressum, {
      localVue,
      vuetify,
      store
    });
    const h1 = wrapper.find("h1");
    expect(h1.is("h1")).toBe(true);
  });
});
