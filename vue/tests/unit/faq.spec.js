import Vue from "vue";
import Vuex from "vuex";
import { state, actions } from "@/store/index";
import Vuetify from "vuetify";
import { mount, createLocalVue } from "@vue/test-utils";
import FAQ from "@/components/FAQ.vue";

Vue.use(Vuetify);

const localVue = createLocalVue();

describe("module", () => {
  let store;
  let vuetify;
  beforeEach(() => {
    vuetify = new Vuetify();
    store = new Vuex.Store({
      getters: {
        loggedIn: () => false,
        activeQuery: () => ""
      },
      state,
      actions
    });
  });

  it("creates component", () => {
    const wrapper = mount(FAQ, {
      localVue,
      vuetify,
      store
    });
    const h2 = wrapper.find("h2");
    expect(h2.is("h2")).toBe(true);
  });
});
