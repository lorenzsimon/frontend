## Documentation of the Frontend:


### What is the main purpose of the Frontend-Service?

The Frontend-Service is the structured visualisation or interface of Boogle. It enables the users to access and work with the other services of the application via buttons, links etc. With the help of certain error messages from the Backend-Services the Frontend-Service also show the user which mistakes he made. Without this service the user would need to access the other parts of Boogle with their own HTTP Requests via a tool like postman and communication in general would be much harder.

Due to the fact that the Frontend-Service was implemented with the framework vue.js it consists of a main page that loads different components when they are needed. All these components will be introduced in the following Internal Logic section.


### Internal Logic of the Query-Service:

The Frontend-Service works with a main page that loads the right components when needed. With the help of the i18n tool for vue, these components are available in german and english. Therefore the components don't contain the text inside the html tags but references to the correct text in the .js files. With the correct routing the correct .js file and thus the correct language is loaded. Another important logical feature is the possibility to change to darkmode via vuetify.

#### Components:

- Admin  
    The Admin component contains the important information for admins of boogle. There admins can see all the registered users. Admins can reset the
    names of these users, set query limits for them or delete them completely.
    Accessible via: /admin  

- ChangePassword  
    The ChangePassword component enables the user to change his password. Here he/she needs to add his/her currenty password and his/her new password
    twice.
    The new password will be checked via a password check and then sent to the auth service.
    Accessible via: /changepassword  

- Chart  
    The Chart component is loaded on the Homepage of Boogle and enables the user to put his data from a search into different charts.
    He/she can than download these charts in several data formats(.png, .csv, .jpeg)
    Accessible via / after a search  

- Data Policy  
    The data policy component contains a data policy that reminds the user of data that he/she gives to boogle
    Accessible via: /datapolicy

- DeleteAccount  
    The deleteAccount component enables the user to delete his account. He/she needs to insert his current password. If this is correct the user will
    be deleted from all other services.
    Accessible via: /deleteaccount

- Error  
    The error component shows a 404 error if the page the user is looking for can not be found.

- FAQ  
    The FAQ component contains a little faq where the user can look up the most frequently asked questions.
    Accessible via: /faq

- Footer  
    The footer component contains links to the imprint and the data policy as well as a copyright note.
    Accessible on every page.

- Forum  
    The forum component enables the user to get in to a discussion with other users. Every logged in user can open up a new posting and also comment on
    other postings.
    Accessible via: /forum

- Highlighted  
    The highlighted component enables highlighting of the Sparql Query while writting. Is also shows the mistakes and how to resolve them.
    Accessible via / in a sparql query search

- Impressum  
    The imprint component contains the imprint of the producers of boogle.
    Accessible via: /impressum

- LanguageSwitcher  
    The language switcher component contains a drop down menu where the user can switch his language between english and german.
    Accessible via the navigation.

- Login  
    The login component enables a registered user to log into Boogle. He/she needs to enter his/her mail and password. This will be checked by the
    auth service and the user will be logged in if the inputs were correct.
    Accessible via: /login

- Navigation  
    The navigation component enables the user to navigate himself to the different parts of boogle like the faq or the login. The navigation also
    looks a bit different for logged in users than for logged out users and the admin also has other naviation parts.
    Accessible on every page.

- OAuth  
    The OAuth component manages the log in with OAuth and our Auth service.
    Accessible via: /oauth

- Profile  
    The profile component shows the name and mail address of the user. This components gets the information form the user and auth service and is also
    able to post changes to these services. On this component the user is therefore able to change his name and mail address.
    Accessible via: /profile

- Register  
    The profile component enables the user to register himself for boogle. There he/she needs to add a name (that is sent to the user service), a mail
    address and a password (those two are sent to the auth service). If the password is strong enough the user is registered in the user and auth 
    service and logged in.
    Accessible via: /register

- Saved  
    The saved component works with the query service and shows all safed queries of a user. It is also the component where the users can delete their
    saved queries or run them again.
    Accessible via: /saved

- Search  
    The search component manages the keyword and sparql search. This component sends the correct searches to the query service and returns a result
    data table.
    Accessible via: /

### Known Bugs

When you use the darkmode and search something on the search page the background is white sometimes. This occurs because of the module for highlighting.
If you look at our login page you will find a button to log in with Guthub. This is actually not a bug but a feature that we implemented on purpose. Due to several problems with the OAuth Github Login we started to call it Guthub and added this to the Frontend.

Manually entering any other url than the one for the main page of the frontend leads to a 404. This problem only occurs when the frontend is deployed as a docker container though, locally starting it makes it work. This also means that the Github (Guthub) login does not work when deployed, as the required mounted call also cannot redirect to the page.

### Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
